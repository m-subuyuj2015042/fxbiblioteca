/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.claudiocanel.ui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Claudio Canel
 */
public class MainWindow extends Application {
    private VBox vBoxPrincipal;
    private MenuBar menuBar;
    private Menu menuArchivo;
    private Menu menuUsuario;
    private MenuItem menuItemConectar;
    private MenuItem menuItemDesconectar;
    private MenuItem menuItemSalir;
    private Stage stage;
    
    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        vBoxPrincipal = new VBox();
        
        vBoxPrincipal.getChildren().add(getMenuBar());
        
        Scene scene = new Scene(vBoxPrincipal, 300, 250);
        
        stage.setTitle("Hello World!");
        stage.setScene(scene);
        stage.show();
    }
    
    private MenuBar getMenuBar() {
        menuBar = new MenuBar();
        
        menuArchivo = new Menu("_Archivo");
        
        menuUsuario = new Menu("_Usuario");
        
        menuItemConectar = new MenuItem("_Conectar");
        
        menuItemDesconectar = new MenuItem("_Desconectar");
        menuUsuario.getItems().addAll(menuItemConectar, menuItemDesconectar);
        
        menuItemSalir = new MenuItem("_Salir");
        menuItemSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        menuArchivo.getItems().addAll(menuUsuario, menuItemSalir);
        menuBar.getMenus().add(menuArchivo);
        return menuBar;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
